package com.ndood.authenticate.browser;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import com.ndood.authenticate.browser.handler.BrowserAuthenticationFailureHandler;
import com.ndood.authenticate.browser.handler.BrowserAuthenticationSuccessHandler;
import com.ndood.authenticate.browser.handler.BrowserLogoutSuccessHandler;
import com.ndood.authenticate.browser.session.BrowserExpiredSessionStrategy;
import com.ndood.authenticate.browser.session.BrowserInvalidSessionStrategy;
import com.ndood.core.properties.SecurityProperties;

/**
 * 浏览器默认Bean配置类
 * @author ndood
 */
@Configuration
public class BrowserSecurityBeanConfig {
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private SecurityProperties securityProperties;
	
	/**
	 * token持久化初始类，自动建表
	 */
	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		BrowserRememberMeJdbcTokenRepositoryImpl tokenRepository = new BrowserRememberMeJdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		// 在启动的时候创建一张表
		// 启动一次自动生成表后注释掉
		// tokenRepository.setCreateTableOnStartup(true);
		return tokenRepository;
	}
	
	/**
	 * session失效时的处理策略配置
	 */
	@Bean
	@ConditionalOnMissingBean(InvalidSessionStrategy.class)
	public InvalidSessionStrategy invalidSessionStrategy(){
		return new BrowserInvalidSessionStrategy(securityProperties);
	}
	
	/**
	 * 并发登录导致前一个session失效时的处理策略配置
	 */
	@Bean
	@ConditionalOnMissingBean(SessionInformationExpiredStrategy.class)
	public SessionInformationExpiredStrategy sessionInformationExpiredStrategy(){
		return new BrowserExpiredSessionStrategy(securityProperties);
	}
	
	/**
	 * 退出登录处理器配置  方便给用户覆盖掉默认的退出登录成功处理器
	 */
	@Bean
	@ConditionalOnMissingBean(LogoutSuccessHandler.class)
	public LogoutSuccessHandler logoutSuccessHandler(){
		return new BrowserLogoutSuccessHandler(securityProperties.getBrowser().getSignOutUrl());
	}
	
	/**
	 * 登录成功处理
	 */
	@Bean
	@ConditionalOnMissingBean(AuthenticationSuccessHandler.class)
	public AuthenticationSuccessHandler authenticationSuccessHandler(){
		return new BrowserAuthenticationSuccessHandler();
	}
	
	/**
	 * 登录失败处理
	 */
	@Bean
	@ConditionalOnMissingBean(AuthenticationFailureHandler.class)
	public AuthenticationFailureHandler authenticationFailureHandler(){
		return new BrowserAuthenticationFailureHandler();
	}
	
}
