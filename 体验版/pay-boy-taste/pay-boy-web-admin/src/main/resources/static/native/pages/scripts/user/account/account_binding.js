define([
	'jquery',
	'jquery-extension',
	'layui',
	'jquery-validation'
], function($) {
	return {
		initPage: function(){
			refresh_binding_status();
		}
	}
});

// 刷新社交登录状态
function refresh_binding_status(){
	var hasBind = 0;
	$.ajax({
    	url:"/connect",
        type:"get",
        dataType:'json',
        data:{},
        async:false,
        success:function(data){
        	var qq_status = data['qq'];
        	var weixin_status = data['weixin'];
        	if(qq_status){
        		$("#qq_icon").removeClass('social-ok').addClass('social-ok');
        		$("#qq_status").text('已绑定').attr("color",'#32c5d2');
        		$('#qq_binding').html('<button class="btn btn-success btn-sm" type="button" onclick="un_binding_qq()">解除绑定</button>');
        		hasBind++;
        	}else{
        		$("#qq_icon").removeClass('social-ok');
        		$("#qq_status").text('未绑定').attr("color",'#32c5d2');
        		$('#qq_binding').html('<button class="btn btn-success btn-sm" type="button" onclick="binding_qq()">绑定</button>');
        	}
        	if(weixin_status){
        		$("#weixin_icon").removeClass('social-ok').addClass('social-ok');
        		$("#weixin_status").text('已绑定').attr("color",'#32c5d2');
        		$('#weixin_binding').html('<button class="btn btn-success btn-sm" type="button"  onclick="un_binding_weixin()">解除绑定</button>');
        		hasBind++;
        	}else{
        		$("#weixin_icon").removeClass('social-ok');
        		$("#weixin_status").text('未绑定').attr("color",'#32c5d2');
        		$('#weixin_binding').html('<button class="btn btn-success btn-sm" type="button"  onclick="binding_weixin()">绑定</button>');
        	}
        	return false;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
	$.ajax({
    	url:"/user/account/binding/get",
        type:"post",
        dataType:'json',
        data:{},
        async:false,
        success:function(res){
        	if(res.code!=10000){
        		console.log(res.msg);
        		return;
        	}
        	var userinfo = res.data;
        	var mobile = userinfo.mobile;
        	var email = userinfo.encryptEmail;
        	var hidden_email = userinfo.email;
        	var emailStatus = userinfo.emailStatus;
        	
        	$("#mobile_icon").removeClass('social-ok');
        	if(mobile){
        		$("#mobile_icon").addClass('social-ok');
        		$("#bind_mobile").text(mobile);
        		$("#mobile_bindstatus").text("已绑定").attr("color",'#32c5d2');
        		$('#mobile_binding').html('<button class="btn btn-success btn-sm" type="button" onclick="change_binding_mobile()">更换绑定</button>');
        		hasBind++;
        	}
        	
        	$("#email_icon").removeClass('social-ok');
        	if(email){
        		$("#bind_email").text(email);
        		$("#hidden_email").text(hidden_email);
        		$("#email_bindstatus").text("已绑定").attr("color",'#32c5d2');
        		if(emailStatus==1){
        			$("#email_icon").addClass('social-ok');
            		$("#email_activestatus").text("已激活").attr("color",'#32c5d2');
            		$('#email_binding').html('<button class="btn btn-success btn-sm" type="button" onclick="change_binding_email()">更换绑定</button>');
            		hasBind++;
        		}else{
        			$('#email_binding').html(
        				'<button class="btn btn-success btn-sm" type="button" onclick="change_binding_email()">更换绑定</button> '+
        				'<button class="btn btn-success btn-sm" type="button" onclick="active_binding_email()">激活</button>');
        		}
        	}
        	return false;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
	
	// 绑定计数
	$("#has_bind").text(hasBind);
}

// 绑定qq
function binding_qq(){
	var url = '/connect/qq';
	var name = '_blank';
	
    var iWidth=800; //弹出窗口的宽度;
    var iHeight=600; //弹出窗口的高度;
    var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
    var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

    var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
    if (!newWindow)  
        return false;  
    var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
    /*  var values = ['1','2'];
    if (keys && values)  
    {  
       html += "<input type='hidden' name='" + keys + "' value='" + values + "'/>";  
    }  */
    html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
    html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
    newWindow.document.write(html);
}

// 解绑qq
function un_binding_qq(){
	layer.prompt({title: '输入密码并确认', formType: 1}, function(pass, index){
		$.ajax({
	    	url:"/user/check_pass",
	        type:"get",
	        dataType:'json',
	        data:{pass:pass},
	        async:false,
	        success:function(data){
	        	if(data.code!='10000'){
	        		layer.msg(data.msg);
	        		layer.close(index);
	        		return false;
	        	}
	        	var url = '/connect/qq';
	        	var name = '_blank';
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        
	            var newWindow = window.open(url, name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow){
	            	layer.close(index);
	            	return false;  
	            }  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            html += "<input type='hidden' name='_method' value='delete'/>";  
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	            layer.close(index);
	        	return false;
	        },
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
	    });
	});
}

// 绑定微信
function binding_weixin(){
	var url = '/connect/weixin';
	var name = '_blank';
	
    var iWidth=800; //弹出窗口的宽度;
    var iHeight=600; //弹出窗口的高度;
    var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
    var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

    var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
    if (!newWindow)  
        return false;  
    var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
    html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
    html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
    newWindow.document.write(html);
}

//解绑微信
function un_binding_weixin(){
	layer.prompt({title: '输入密码并确认', formType: 1}, function(pass, index){
		$.ajax({
	    	url:"/user/check_pass",
	        type:"get",
	        dataType:'json',
	        data:{pass:pass},
	        async:false,
	        success:function(data){
	        	if(data.code!='10000'){
	        		layer.msg(data.msg);
	        		layer.close(index);
	        		return false;
	        	}
	        	var url = '/connect/weixin';
	        	var name = '_blank';
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        
	            var newWindow = window.open(url, name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow){
	            	layer.close(index);
	            	return false;  
	            }  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            html += "<input type='hidden' name='_method' value='delete'/>";  
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	            layer.close(index);
	        	return false;
	        },
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
	    });
	});
}

/**
 * 绑定手机
 */
function binding_mobile(){
	var url = '/user/account/binding/to_new_binding';
	var name = '_blank';
	
    var iWidth=800; //弹出窗口的宽度;
    var iHeight=600; //弹出窗口的高度;
    var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
    var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

    var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
    if (!newWindow)  
        return false;  
    var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
    html += "<input type='hidden' name='type' value='mobile'/>";  
    html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
    html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
    newWindow.document.write(html);
}

/**
 * 更换绑定手机
 */
function change_binding_mobile(){
	layer.prompt({title: '输入原手机号并确认', formType: 1}, function(mobile, index){
		$.ajax({
	    	url:"/user/account/binding/check_old_mobile",
	        type:"get",
	        dataType:'json',
	        data:{mobile:mobile},
	        async:false,
	        success:function(data){
	        	if(data.code!='10000'){
	        		layer.msg(data.msg);
	        		layer.close(index);
	        		return false;
	        	}
	        	var url = '/user/account/binding/to_change_binding';
	        	var name = '_blank';
	        	
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

	            var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow)  
	                return false;  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            html += "<input type='hidden' name='type' value='mobile'/>";  
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	        	layer.close(index);
	        	return false;
	        },
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
	    });
	});
}

/**
 * 绑定邮箱
 */
function binding_email(){
	var url = '/user/account/binding/to_new_binding';
	var name = '_blank';
	
    var iWidth=800; //弹出窗口的宽度;
    var iHeight=600; //弹出窗口的高度;
    var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
    var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

    var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
    if (!newWindow)  
        return false;  
    var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
    html += "<input type='hidden' name='type' value='email'/>";  
    html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
    html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
    newWindow.document.write(html);
}

/**
 * 更换绑定邮箱
 */
function change_binding_email(){
	layer.prompt({title: '输入密码并确认', formType: 1}, function(pass, index){
		$.ajax({
	    	url:"/user/check_pass",
	        type:"get",
	        dataType:'json',
	        data:{pass:pass},
	        async:false,
	        success:function(data){
	        	if(data.code!='10000'){
	        		layer.msg(data.msg);
	        		layer.close(index);
	        		return false;
	        	}
	        	var url = '/user/account/binding/to_change_binding';
	        	var name = '_blank';
	        	
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

	            var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow)  
	                return false;  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            html += "<input type='hidden' name='type' value='email'/>";  
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	        	layer.close(index);
	        	return false;
	        },
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
	    });
	});
}

/**
 * 激活绑定邮箱，重新发送激活邮件
 */
function active_binding_email(){
	var email_send_to = $("#hidden_email").text();
	$.ajax({
    	url:"/user/account/binding/resend_active_email",
        type:"post",
        dataType:'json',
        data:{email:email_send_to},
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		layer.alert(data.msg);
        		return false;
        	}
        	var hash={ 
    			'qq.com': 'http://mail.qq.com', 
    			'gmail.com': 'http://mail.google.com', 
    			'sina.com': 'http://mail.sina.com.cn', 
    			'163.com': 'http://mail.163.com', 
    			'126.com': 'http://mail.126.com', 
    			'yeah.net': 'http://www.yeah.net/', 
    			'sohu.com': 'http://mail.sohu.com/', 
    			'tom.com': 'http://mail.tom.com/', 
    			'sogou.com': 'http://mail.sogou.com/', 
    			'139.com': 'http://mail.10086.cn/', 
    			'hotmail.com': 'http://www.hotmail.com', 
    			'live.com': 'http://login.live.com/', 
    			'live.cn': 'http://login.live.cn/', 
    			'live.com.cn': 'http://login.live.com.cn', 
    			'189.com': 'http://webmail16.189.cn/webmail/', 
    			'yahoo.com.cn': 'http://mail.cn.yahoo.com/', 
    			'yahoo.cn': 'http://mail.cn.yahoo.com/', 
    			'eyou.com': 'http://www.eyou.com/', 
    			'21cn.com': 'http://mail.21cn.com/', 
    			'188.com': 'http://www.188.com/', 
    			'foxmail.com': 'http://www.foxmail.com' 
    		};
    		if(email_send_to){
    			var url = email_send_to.trim().split('@')[1];
    			window.location.href = hash[url];
    		}
        	return false;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}