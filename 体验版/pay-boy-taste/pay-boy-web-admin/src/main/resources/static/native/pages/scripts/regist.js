define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'bootstrap',
	'layui'
], function($) {
	return {
		init: function() {
			// 设置默认同意勾选
			$('#regist_agree').prop('checked',true);
			
	    	// 初始化jquery验证插件
	        $('#regist_form').validate({
	            errorElement: 'span', // 默认错误显示位置
	            errorClass: 'help-block', // 默认错误显示样式
	            focusInvalid: true, // 获得焦点时是否验证
	            // 验证规则
	            rules: {
	            	username:{
	            		required: true
	            	},
	            	imageCode:{
	            		required: true
	            	}
	            },
	            // 验证提示
	            messages: {
	            	username:{
	            		required: '手机号/邮箱不能为空！'
	            	},
	            	imageCode:{
	            		required: '验证码不能为空！'
	            	},
	            	
	            },
	            // 只要不成功，就显示.alert-dander div
	            invalidHandler: function(event, validator) {
	                $('.alert-danger', $('#regist_form')).show();
	            },
	            // 错误输入框高亮显示
	            highlight: function(element) { 
	                $(element).closest('.form-group').removeClass('valid').addClass('has-error'); 
	            },
	            // 验证成功，移除错误样式
	            success: function(label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },
	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
	            	if(error.text()!=''){
	            		console.log(error.text());
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
	        });
	        
	        // 监听回车事件
	        $('#regist_form input').keypress(function(e) {
	            if (e.which == 13) {
	                if ($('#regist_form').validate().form()) {
	                	$('#regist_submit').click();
	                }
	                return false;
	            }
	        });

	        // 验证码
	        $('#regist_captcha').click(function() {
	            var $this = $(this);
	            var url = $this.data('src')+'&'+ new Date().getTime();
	            $this.attr('src', url);
	        });

	        // 如果是邮箱登录，需要输入密码。如果是短信登录暂时不用输入密码
	        $('#regist_form').find('input[name="username"]').blur(function(){
	        	var value = $(this).val();
	        	var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	        	if(reg.test(value)){
	        		$('#regist_password_mask').val('').show();
	        	}else{
	        		$('#regist_password_mask').val('').hide();
	        	}
	        });
	        
	        $('#regist_submit').click(function(){
	        	// Step1: form表单校验，远程校验用户名是否被占用。密码字段根据username判断，如果是邮箱注册则需要进行校验
	        	var valid = $('#regist_form').valid();
	        	$('#regist_form').find('input[name="imageCode"]').valid();
	        	$('#regist_form').find('input[name="username"]').valid();
	        	if(!valid){
	        		return;
	        	}
	        	
	        	// Step2: 是否同意注册协议校验
	        	var value = $('#regist_form').find('input[name="username"]').val();
	        	var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	        	var isMobile = true;
	        	var passValid = true; 
	        	var pass = $('#regist_form').find('input[name="password"]').val();
	        	if(reg.test(value)){
	        		isMobile = false;
	        		if(pass.trim()==''){
	        			passValid = false;
	        		}
	        	}
	        	if(!passValid){
	        		$('.alert-danger','#regist_form').show().find('span').text('密码不能为空！');
	        		return;
	        	}
	        	
	        	// Step3: 判断是否已经同意注册协议
	        	var checked = $('#regist_agree').prop('checked');
	        	if(!checked){
	        		$('.alert-danger','#regist_form').show().find('span').text('同意payboy用户协议才能继续注册！')
	        		return;
	        	}
	        	
	        	// Step4: 进行半注册，注册成功后跳转到另一半注册
	        	$('.alert-danger', $('#regist_form')).val('').hide();
	        	
	        	var data =  $('#regist_form').serializeJson();
	        	var url = '/user/regist/mobile';
	        	if(!isMobile){
	        		url = '/user/regist/email';
	        		data['password'] = pass;
	        	}
	        	$('#regist_submit').button('loading');
	        	$.ajax({
	            	url: url,
	                type:'post',
	                dataType:'json',
	                contentType:'application/x-www-form-urlencoded',
	                data: data,
	                async:true,
	                success:function(data){
	                	$('#regist_submit').button('reset')
	                	// 处理注册失败
	                	if(data.code!='10000'){
                    		$('.alert-danger', $('#regist_form')).find('span').html(data.msg);
                    		$('.alert-danger', $('#regist_form')).show(); 
                    		$('#regist_captcha').click();
                    		return false;
                    	}
	                	// 如果注册成功，是短信注册，则跳转到短信注册的下一页
	                	if(isMobile){
	                		goToMobileRegist(value);
	                		return false;
	                	}
	                	// 如果是邮箱注册，则跳转到有效注册的下一页
	                	goToEmailRegist(value);
	                	return false;
	                },
	                // 自定义错误提示位置
		            errorPlacement: function(error, element) {
		            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
		            	if(error.text()!=''){
		            		console.log(error.text());
		            		$(errorDom).text(error.text());
		            	}
		            }
	            });
	        });	
	        	
        	// --------------------------------/短信注册后续逻辑-------------------------------------
        	// 表单验证初始化
        	$('#mobile_form').validate({
	            errorElement: 'span', // 默认错误显示位置
	            errorClass: 'help-block', // 默认错误显示样式
	            focusInvalid: true, // 获得焦点时是否验证
	            // 验证规则
	            rules: {
	            	password:{
	            		required: true
	            	},
	            	smsCode:{
	            		required: true
	            	}
	            },
	            // 验证提示
	            messages: {
	            	password:{
	            		required: '密码不能为空！'
	            	},
	            	smsCode:{
	            		required: '短信验证码不能为空！'
	            	},
	            },
	            // 只要不成功，就显示.alert-dander div
	            invalidHandler: function(event, validator) {
            		$('.alert', $('#mobile_form')).hide()
            		$('.alert-danger', $('#mobile_form')).show(); 
	            },
	            // 错误输入框高亮显示
	            highlight: function(element) { 
	                $(element).closest('.form-group').removeClass('valid').addClass('has-error'); 
	            },
	            // 验证成功，移除错误样式
	            success: function(label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },
	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
	            	if(error.text()!=''){
	            		console.log(error.text());
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
	        });

        	// 获取验证码 多次返回修改手机号后回来重新发送验证码，多次发送的问题
        	$('#mobileSmsCodeBtn').click(function() {
        		var mobile = $('#regist_form').find('input[name="username"]').val();
	    		$.ajax({
		        	url: '/code/sms?mobile='+mobile,
		        	type: 'GET',
		        	async: false,
		        	data: {},
		        	success: function(data) {
		                if(data.code=='10000') {
		                	$('#mobileTotalSecond').val(31);
		                	$('#mobileSmsCodeBtn').attr('disabled', true);
		                	$('#mobileSmsCode').val(data.content);
		                	
		                	// 启动定时任务，如果不存在则新建
		                	var intervalId = $('#mobileIntervalId').val();
		                	if(!intervalId){
		                		var interval = setInterval(getOneMoreSMS, 1000);
		                		$('#mobileIntervalId').val(interval);
		                	}
		                } else {
		                	layer.alert('短信发送失败!')
		                }
		        	},
		        	// 自定义错误提示位置
		            errorPlacement: function(error, element) {
		            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
		            	if(error.text()!=''){
		            		console.log(error.text());
		            		$(errorDom).text(error.text());
		            	}
		            }
		        });
	        });
	        var getOneMoreSMS = function() {
	        	var second = $('#mobileTotalSecond').val();
	    		if (second == 1) {
	    			var intervalId = $('#mobileIntervalId').val();
                	if(intervalId){
                		$('#mobileIntervalId').val('');
                		clearInterval(intervalId);
                	}
	    			$('#mobileSmsCodeBtn').html('获取短信验证码')
	    			$('#mobileSmsCodeBtn').attr('disabled', false);
	    		} else {
	    			var s = second - 1;
	    			$('#mobileTotalSecond').val(s);
	    			$('#mobileSmsCodeBtn').html('获取短信验证码(' + s + '秒)')
	    		}
	    	}
	        
	        // 完成手机注册
	        $('#mobile_submit').click(function(){
	        	var valid = $('#mobile_form').valid();
	        	$('#mobile_form').find('input[name="smsCode"]').valid();
	        	$('#mobile_form').find('input[name="password"]').valid();
	        	if(!valid){
	        		return;
	        	}
	        	
	        	var data =  $('#mobile_form').serializeJson();
	        	var mobile = $('#regist_form').find('input[name="username"]').val();
	        	data['mobile'] = mobile;
	        	$('#mobile_submit').button('loading');
	        	$.ajax({
	            	url: '/user/regist/mobile_finish',
	                type:'post',
	                dataType:'json',
	                contentType:'application/x-www-form-urlencoded',
	                data: data,
	                async:true,
	                success:function(data){
	                	$('#mobile_submit').button('reset')
	                	// 处理注册失败
	                	if(data.code!='10000'){
	                		$('.alert', $('#mobile_form')).hide()
                    		$('.alert-danger', $('#mobile_form')).find('span').html(data.msg);
                    		$('.alert-danger', $('#mobile_form')).show(); 
                    		return false;
                    	}
	                	// 注册成功后跳转到登录页
	                	/*layer.confirm('注册成功，马上去登录!', {icon: 1, title:'提示',yes: function(index){
	                		window.location.href = '/signOut';
		                },
	                	cancel: function(index, layer){ 
		                	window.location.href = '/signOut';
	                	}});*/
	                	window.location.href = '/user/regist/to_success?type=mobile&username='+mobile;
	                	return false;
	                },
	                // 自定义错误提示位置
		            errorPlacement: function(error, element) {
		            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
		            	if(error.text()!=''){
		            		console.log(error.text());
		            		$(errorDom).text(error.text());
		            	}
		            }
	            });
	        });
        	// --------------------------------短信注册后续逻辑/-------------------------------------
        	
	        // --------------------------------/邮箱注册后续逻辑-------------------------------------
	        $("#email_submit").click(function(){
	        	var hash={ 
    				'qq.com': 'http://mail.qq.com', 
    				'gmail.com': 'http://mail.google.com', 
    				'sina.com': 'http://mail.sina.com.cn', 
    				'163.com': 'http://mail.163.com', 
    				'126.com': 'http://mail.126.com', 
    				'yeah.net': 'http://www.yeah.net/', 
    				'sohu.com': 'http://mail.sohu.com/', 
    				'tom.com': 'http://mail.tom.com/', 
    				'sogou.com': 'http://mail.sogou.com/', 
    				'139.com': 'http://mail.10086.cn/', 
    				'hotmail.com': 'http://www.hotmail.com', 
    				'live.com': 'http://login.live.com/', 
    				'live.cn': 'http://login.live.cn/', 
    				'live.com.cn': 'http://login.live.com.cn', 
    				'189.com': 'http://webmail16.189.cn/webmail/', 
    				'yahoo.com.cn': 'http://mail.cn.yahoo.com/', 
    				'yahoo.cn': 'http://mail.cn.yahoo.com/', 
    				'eyou.com': 'http://www.eyou.com/', 
    				'21cn.com': 'http://mail.21cn.com/', 
    				'188.com': 'http://www.188.com/', 
    				'foxmail.com': 'http://www.foxmail.com' 
	        	};
	        	var email_send_to = $("#email_send_to").val();
	        	if(email_send_to){
	        		var url = email_send_to.split('@')[1];
	        		window.location.href = hash[url];
	        	}
	        });
        	// --------------------------------邮箱注册后续逻辑/-------------------------------------
	        
        	// --------------------------------/其它方式验证后续逻辑-------------------------------------
	        // TODO 邮箱未激活的情况下使用短信验证
	        // 如果该手机已经使用
	        	// 如果该手机已经绑定邮箱，则提示验证失败，手机已经绑定邮箱
	        	// 如果手机未绑定邮箱，则以手机账号为主账号将2个账号合并
	        // 如果该手机未使用，将手机绑定到邮箱对应的账号
	        // --------------------------------其它方式验证后续逻辑/-------------------------------------
	        
	        // qq登录
	        $("#regist-qq-login").off('click');
	        $("#regist-qq-login").click(function(){
	        	var url = '/qqLogin/qq';
	        	var name = '_blank';
	        	
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

	            var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow)  
	                return false;  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            /*  var values = ['1','2'];
	            if (keys && values)  
	            {  
	               html += "<input type='hidden' name='" + keys + "' value='" + values + "'/>";  
	            }  */
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	        });
	        
	        // 微信登录
	        $("#regist-wechat-login").off('click');
	        $("#regist-wechat-login").click(function(){
	        	var url = '/qqLogin/weixin';
	        	var name = '_blank';
	        	
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

	            var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow)  
	                return false;  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	        });
		}
	}
});

// 短信注册
function goToMobileRegist(mobile){
	$('.regist-content').find('form').hide();
	$('#mobile_form')[0].reset();
	
	$('.alert', $('#mobile_form')).hide()
	$('#mobile_form_success_tip').find('span').html('注册成功，请填写密码和验证码完成验证！<br/>短信验证码发送至 '+mobile)
	$('#mobile_form_success_tip').show();
	$('#mobile_form').show();
}

// 邮箱注册
function goToEmailRegist(email){
	$('.regist-content').find('form').hide();
	$('#email_form')[0].reset();
	$("#email_send_to").val(email); // 设置激活邮箱地址
	
	$('.alert', $('.email_form')).hide()
	$('#email_form_success_tip').find('span').html('注册成功，填写激活邮件完成注册！<br/>邮件发送至 '+email);
	$('#email_form_success_tip').show();
	$('#email_form').show();
}

// 短信注册
function goToRegist(){
	$('.regist-content').find('form').hide();
	$('#regist_form')[0].reset();
	$('#regist_form').show();
	$('#regist_agree').prop('checked',true);
	$('#regist_captcha').click();
}