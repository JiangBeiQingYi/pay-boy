package com.ndood.admin.repository.system;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.DictPo;

/**
 * https://docs.spring.io/spring-data/jpa/docs/2.0.3.RELEASE/reference/html/
 * https://spring.io/blog/2011/04/26/advanced-spring-data-jpa-specifications-and-querydsl/
 * 数据字典dao
 */
public interface DictRepository extends JpaRepository<DictPo, Integer>{

	Page<DictPo> findAll(Specification<DictPo> specification, Pageable pageable);
	
}
