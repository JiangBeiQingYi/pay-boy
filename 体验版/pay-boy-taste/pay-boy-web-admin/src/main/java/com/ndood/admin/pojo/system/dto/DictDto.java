package com.ndood.admin.pojo.system.dto;

import com.ndood.admin.pojo.system.DictPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 角色DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class DictDto extends DictPo {
	private static final long serialVersionUID = 6650285921381454775L;
}
