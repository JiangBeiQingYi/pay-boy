package com.ndood.admin.service.system;

import java.util.List;

import com.ndood.admin.pojo.system.dto.PermissionDto;

/**
 * 资源管理业务接口
 * @author ndood
 */
public interface SystemPermissionService {

	/**
	 * 获取资源tree列表
	 * @param keywords 
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	List<PermissionDto> getPermissionList() throws Exception;

	/**
	 * 批量删除资源
	 * @param ids
	 */
	void batchDeletePermission(Integer[] ids);

	/**
	 * 添加资源
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	PermissionDto addPermission(PermissionDto dept) throws Exception;

	/**
	 * 获取资源信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	PermissionDto getPermission(Integer id) throws Exception;

	/**
	 * 更新资源信息
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	PermissionDto updatePermission(PermissionDto dept) throws Exception;

}
